#include<stdio.h>
#include<stdlib.h>

struct node
{
    int data;
    struct node* left;
    struct node* right;
};
struct node* createNode(int value) {
    struct node* newNode =(node *) malloc(sizeof(struct node) * 10);
    newNode->data = value;
    newNode->left = NULL;
    newNode->right = NULL;
    
	return newNode;
}
  
struct node* insert(struct node* root, int data)
{
    if (root == NULL) 
		return createNode(data);
		
    else if (data < root->data)
        	root->left  = insert(root->left, data);
    
	else if (data > root->data)
        	root->right = insert(root->right, data);   
  
    return root;
}

void inorder(struct node* root) {
    if(root == NULL) return;
    inorder(root->left);
    printf("%d ->", root->data);
    inorder(root->right);
}


void preorder(struct node* root) {
    if(root == NULL) return;
    printf("%d ->", root->data);
    preorder(root->left);
    preorder(root->right);	
}

void postorder(struct node* root) {
    if(root == NULL) return;
    postorder(root->left);
    postorder(root->right);
    printf("%d ->", root->data); 
}


int Search(struct node* root, int num){
	
	if (root == NULL)
		return NULL;
		
	else if (num == root->data){
			
		return root->data;
	}
	else if (num < root->data){
	
		return Search(root->left, num);
	}
	else if (num > root->data){
	
		return Search(root->right, num);
	}
}

void contaFoglieNodi(struct node* root,float &somma, int &CN, int &CF){
	
	if (root->left == NULL && root->right == NULL){
		CF++;
		somma = somma + root->data;
	}
	else{
		CN++;
	}
	
	if (root->left != NULL){
		contaFoglieNodi(root->left, somma, CN, CF);
	}
	
	if (root->right != NULL){
		contaFoglieNodi(root->right, somma, CN, CF);
	}
	
}

float mediaFoglie(float somma, int CF){

	int media = 0;
	
	media = somma / CF;
	
	return media;
}

int main(){
	int  CN = 0, CF = 0, D = 0;
	int number = 0, i = 0, dim = 0;
	float media = 0, somma = 0;
	
	struct node* root = NULL;
	printf ("inserisci il root :_ ");
	scanf ("%d", &D);
	root = insert(root, D);
	printf("quanti nodi vo�uoi inserire :_ ");
	scanf("%d", &dim);
	
	for (; i < dim ; i++){
		printf("in nodo numero [%d]", i);
		scanf("%d", &number);
		insert(root, number);
	}
	inorder(root);
	
	contaFoglieNodi(root, somma, CN, CF);
	printf("\nLa somma delle foglie e :_ [%f]\nLe foglie sono :_ [%d]\nI nodi sono :_ [%d]", somma, CF, CN);
	printf("\nLa media delle foglie e :_ [%.1f]", mediaFoglie(somma, CF));
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	/*------------------------*/
	/*root = insert(root, 50);
	insert(root, 78);
	insert(root, 60);
	insert(root, 44);
	insert(root, 38);
	insert(root, 45);
	insert(root, 12);
	insert(root, 79);*/
	/*------------------------*/
	/*inorder(root);
	contaFoglieNodi(root, somma, CN, CF);
	mediaFoglie(somma, CF);*/
	/*------------------------*/
	/*printf("\nLa somma delle foglie:_ [%f]\n"
		   "Il numero delle foglie:_ [%d]\n"
		   "Il numero dei nodi:_ [%d]\n"
		   "Media foglie:_ [%f]\n", somma, CF, CN, media);*/
	return 0;
}
