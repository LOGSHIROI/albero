#include<iostream>
#include<stdio.h>
#include<stdlib.h>
#include<string.h>
#define dim 100
using namespace std;


struct node{
	
	string data;
	struct node *left;
	struct node *right;
	
};

struct node* creaNodo(char *value){
	struct node* newNode = (node *)malloc(sizeof(struct node) * dim);
	newNode->data = value;
	newNode->left = NULL;
	newNode->right = NULL;
	
	return newNode;
}

struct node *insertData(struct node* root, string *data){
	
	if (root == NULL){
		return creaNodo(data);
	}
	
	else if (strcmp(data, root->data)<0){
		root->left = insertData(root->left, data);
	}
	
	else if (strcmp(data, root->data)>0){
		root->right = insertData(root->right, data);
	}
	
	return root;
	
}

void inorder(struct node* root){
	if (root == NULL){
		return;
	}
	
	inorder(root->left);
	
	cout<<"<"<<root->data<<">"<<" --- ";
	
	inorder(root->right);

}


char Search(struct node* root, char *number){
	
	if (root == NULL){
		return NULL;
	}
	else if (number == root->data){
		return root->data;
	}
	else if (number < root->data){
		return Search(root->left, number);
	}
	else if(number >root->data){
		return Search(root->right, number);
	}
	
}



int main(){
	struct node* root = NULL;
	root = insertData(root, "Mohammad");
	insertData(root, "imad");
	insertData(root, "omar");
	insertData(root, "semeraro");
	insertData(root, "ahmad");
	insertData(root, "elmsalm");
		
	return 0;
}
