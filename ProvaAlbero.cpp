#include<stdio.h>
#include<stdlib.h>
#define dim 100
struct node
{
    int data;
    struct node* left;
    struct node* right;
};
struct node* createNode(int value) {
    struct node* newNode =(node *) malloc(sizeof(struct node) * dim);
    newNode->data = value;
    newNode->left = NULL;
    newNode->right = NULL;
    
	return newNode;
}
  
struct node* insert(struct node* root, int data)
{
    if (root == NULL) 
		return createNode(data);
		
    else if (data < root->data)
        	root->left  = insert(root->left, data);
    
	else if (data > root->data)
        	root->right = insert(root->right, data);   
  
    return root;
}

void inorder(struct node* root) {
    if(root == NULL) return;
    inorder(root->left);
    printf("%d ->", root->data);
    inorder(root->right);
}


void preorder(struct node* root) {
    if(root == NULL) return;
    printf("%d ->", root->data);
    preorder(root->left);
    preorder(root->right);	
}

void postorder(struct node* root) {
    if(root == NULL) return;
    postorder(root->left);
    postorder(root->right);
    printf("%d ->", root->data); 
}


int Search(struct node* root, int num){
	
	if (root == NULL)
		return NULL;
		
	else if (num == root->data){
			
		return root->data;
	}
	else if (num < root->data){
	
		return Search(root->left, num);
	}
	else if (num > root->data){
	
		return Search(root->right, num);
	}
}

void contaFoglieNodi(struct node* root,float &somma, int &CN, int &CF){
	
	if (root->left == NULL && root->right == NULL){
		CF++;
		somma = somma + root->data;
	}
	else{
		CN++;
	}
	
	if (root->left != NULL){
		contaFoglieNodi(root->left, somma, CN, CF);
	}
	
	if (root->right != NULL){
		contaFoglieNodi(root->right, somma, CN, CF);
	}
	
}

float mediaFoglie(float somma, int CF){

	float media = 0;
	
	media = somma / CF;
	
	return media;
}



int main(){
	int number = 0, S = 0 ,E = 0;
	int CF = 0, CN = 0, num = 0;
	float somma = 0, media = 0;
		
	struct node *root = NULL;
	root = insert(root, 50);
    insert(root, 38);
    insert(root, 70);
    insert(root, 43);
    insert(root, 28);
    insert(root, 67);
    insert(root, 49);
    insert(root, 12);
	insert(root, 78);
	inorder(root);
	media = mediaFoglie(somma, CF);
	printf("\n%f", media);
	
	   /* do{
	    	S = 0;
	    	printf("1.Insert number\n2.Show inorder sort\n"
			"3.Show preorder sort\n4.Show postorder sort\n"
			"5.the number of the leafs and nodes\n"
			"6.the media\n7.Search\n8.Exit\nMake a choice:_ ");
	    	scanf("%d", &S);
			if (S == 1){
				printf("Number:_");
	    		scanf("%d", &number);
				insert(root, number);
			}
			else if (S == 2){
				inorder(root);
			}
			else if (S == 3){
				preorder(root);
			}
			else if (S == 4){
				postorder(root);
			}
			else if (S == 5){
				contaFoglieNodi(root, somma, CN, CF);
				printf("Nodi:_ %d\nFoglie:_ %d\nSomma:_ %f", CN, CF, somma);
			}	
			else if (S == 6){
				media = mediaFoglie(somma, CF);
				printf("\n%f", media);
			}
			else if (S == 7){
				printf("Write the key to search in the tree:_ ");
				scanf("%d", &number);
				Search(root, num);
				if (num == number){
					printf("The key found into the tree .");
				}
				else {
					printf("The key not found.");
				}
			}	
			printf("\ndo you want to retry ? \n1.YES\n2.NO \nMake a choice:_ ");
			scanf("%d", &E);
		}while(E == 1);
		*/
	return 0;
}
